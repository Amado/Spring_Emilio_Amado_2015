package persistencia.interfaces;

import java.util.List;

import org.hibernate.HibernateException;

import entidades.Contacto;
import exceptions.DatabaseException;

public interface ContactosDAO {
	
    void actualizaContacto(Contacto contacto) throws HibernateException;

    void eliminaContacto(Contacto contacto) throws HibernateException;

    long guardaContacto(Contacto contacto) throws DatabaseException;

    Contacto obtenContacto(long idContacto) throws HibernateException;

    List<Contacto> obtenListaContactos() throws HibernateException;

}
