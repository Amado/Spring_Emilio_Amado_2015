package main;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;

import beans.HelloWorld;

public class Main {

	public static void main(String[] args) {
		
		ConfigurableApplicationContext  context = new AnnotationConfigApplicationContext
				("beans", "eventsListeners");
		
		
		context.start();

		HelloWorld helloWorld = context.getBean("helloWorld", HelloWorld.class);
//		helloWorld.setMessage("Hello World! from Spring instance");
		helloWorld.getMessage();
		
		context.stop();
		context.close();
	
	}
}
