package com.beans;

import java.util.ArrayList;
import java.util.Iterator;


public class University {
	
	private ArrayList<Student> enrolledStudents;
	private ArrayList<Student> licensedStudents;
	
	public University() {
		enrolledStudents = new ArrayList<Student>();
		licensedStudents = new ArrayList<Student>();
	}

	public boolean enrollStudent(Student newStudent) {
		if( enrolledStudents.isEmpty() ){
			enrolledStudents.add(newStudent);
			postStudentEnrolledMessageSuccess(newStudent);
			return true;
		} else {
			Iterator<Student> it = enrolledStudents.iterator();
			while (it.hasNext()) {
				Student currentStudent = it.next();
				if (currentStudent.getSsNumber().equals(
						newStudent.getSsNumber())) {
					postStudentEnrolledMessageFail(newStudent);
					return false;
				}
			}
			postStudentEnrolledMessageSuccess(newStudent);
			enrolledStudents.add(newStudent);
			return true;
		}
	}

	public boolean licenseStudent(Student newStudent) {
		if( licensedStudents.isEmpty() ){
			licensedStudents.add(newStudent);
			postStudentLicensedMessageSuccess(newStudent);
			return true;
		} else {
			Iterator<Student> it = licensedStudents.iterator();
			while (it.hasNext()) {
				Student currentStudent = it.next();
				if (currentStudent.getSsNumber().equals(
						newStudent.getSsNumber())) {
					postStudentLicensedMessageFail(newStudent);
					return false;
				}
			}
			postStudentLicensedMessageSuccess(newStudent);
			licensedStudents.add(newStudent);
			return true;
		}
	}

	public void throwException() {
//		System.err.println("\nException raised, operation failed\n");
		throw new IllegalArgumentException();
	}

	public void postStudentEnrolledMessageSuccess(Student newStudent) {
		System.out.println("\nUniversity method message : \n\tStudent with ss number : " + "\"" + 
				newStudent.getSsNumber() + "\"" + 
				" enrolling success\n");
	}

	public void postStudentEnrolledMessageFail(Student newStudent) {
		System.out.println("\nUniversity method message : \n\t\nStudent with ss number : " + "\"" + 
				newStudent.getSsNumber() + "\"" + 
				" enrolling fail\n");
	}

	public void postStudentLicensedMessageSuccess(Student newStudent) {
		System.out.println("\nUniversity method message : \n\t\nStudent with ss number : " + "\"" + 
				newStudent.getSsNumber() + "\"" + 
				" licensing fail\n");
	}

	public void postStudentLicensedMessageFail(Student newStudent) {
		System.out.println("\nUniversity method message : \n\t\nStudent with ss number : " + "\"" + 
				newStudent.getSsNumber() + "\"" + 
				" licensing fail\n");
	}

}
