Hi, I'm Emilio Amado ,  I�m a programming enthusiastic, especially about the services management. 
I developed this Spring manual to be a friendly and direct way to learn this complex platform.

Anyone is free to share this project, but if you share it , 
please mention me as the the project creator and indicates my contact email.

For any questions or suggestions, this is my contact email :

	emilioav.informatica@gmail.com


Project description :

Spring full tutorial, made by myself based on different resources. Language Spanish.


Contents: 

Many fully functional eclipse projects.

Pdf documents with code snippets and step by step explanations. 

Spring libraries.


You can track my projects at : https://www.linkedin.com/in/emilioamado

Cheers and share this!