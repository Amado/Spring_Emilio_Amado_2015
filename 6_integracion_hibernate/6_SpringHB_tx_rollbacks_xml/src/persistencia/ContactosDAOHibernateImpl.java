package persistencia;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entidades.Contacto;
import exceptions.DatabaseException;
import persistencia.interfaces.ContactosDAO;

public class ContactosDAOHibernateImpl implements ContactosDAO {

	private Session sesion;

	private SessionFactory sessionFactory;

	// Spring se encargar� de crear el SessionFactory, por lo que necesita de un setter
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void actualizaContacto(Contacto contacto) throws HibernateException {
		try {
			iniciaOperacion();
			sesion.update(contacto);
		} catch (HibernateException he) {
			throw he;
		}
	}

	@Override
	public void eliminaContacto(Contacto contacto) throws HibernateException {
		try {
			iniciaOperacion();
			sesion.delete(contacto);
		} catch (HibernateException he) {
			throw he;
		}
	}

	@Override
	public long guardaContacto(Contacto contacto) throws DatabaseException {
		long id = 0;
		try {
			iniciaOperacion();
			id = (Long) sesion.save(contacto);
			//c�digo a�adido para hacer que el m�todo falle 
	        if(true){
	            throw new DatabaseException("oops este contacto no se guardar�");
	        }
	        //////////////////////////////////////////////
		} catch (HibernateException he) {
			throw he;
		}
		return id;
	}

	@Override
	public Contacto obtenContacto(long idContacto) throws HibernateException {
		Contacto contacto = null;
		try {
			iniciaOperacion();
			contacto = (Contacto) sesion.get(Contacto.class, idContacto);
		} 
		catch (HibernateException he) {
	          throw he;
		}

		return contacto;
	}

	@Override
	public List<Contacto> obtenListaContactos() throws HibernateException {
		List<Contacto> listaContactos = null;

		try {
			iniciaOperacion();
			listaContactos = sesion.createQuery("from Contacto").list();
		} 
		catch (HibernateException he) {
          throw he;
		}

		return listaContactos;
	}

	// Inicia la session de Hibernate y una transacci�jn sobre la DB
	private void iniciaOperacion() throws HibernateException {
		sesion = sessionFactory.getCurrentSession();
	}
	
}
