package beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service(value="servicioRemoto")
public class ServicioRemoto {
	
    private Proceso proceso;
    
//    @Value(value="5")
    private Integer repeticiones;
    
    public ServicioRemoto(){
    }

    @Autowired
    public ServicioRemoto(@Qualifier("calculo")Proceso proceso){
        this.proceso = proceso;
    }
    
    public Object consultaDato(){
    	String resultado = "";
    	
    	for(int i = 0; i < repeticiones; i++){
    		resultado = resultado + (i+1) + " - " + proceso.ejecuta().toString() + "\n";
    	}
    	
        return resultado;
    }
    
    @Value(value="5")
    public void setRepeticiones(Integer repeticiones){
     this.repeticiones = repeticiones;
    }
    
}
