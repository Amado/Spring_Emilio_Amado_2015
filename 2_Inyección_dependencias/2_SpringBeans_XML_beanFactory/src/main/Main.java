package main;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

import beans.ServicioRemoto;

public class Main {

    public static void main(String[] args) {
    	
        BeanFactory beanFactory = new XmlBeanFactory(new ClassPathResource("applicationContext.xml"));
        
        //con nuestro "BeanFactory" podemos obtener un bean haciendo uso del identificador que 
        //le dimos en el archivo XML, en este caso "servicioRemoto"
        ServicioRemoto servicio = beanFactory.getBean("servicioRemoto", ServicioRemoto.class);
        
        //Ahora que ya tenemos una instancia de "ServicioRemoto" podemos invocar sus m�todos como lo hacemos normalmente
        System.out.println("El valor es " + servicio.consultaDato());
    }
}
