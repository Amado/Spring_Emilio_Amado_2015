package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.Estudiante;
import beans.Universitario;

public class Main {

    public static void main(String[] args) {
    	
    	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        
        Estudiante estudianteUniversitario1 = applicationContext.getBean("estudianteUniversitario", Estudiante.class);
        System.out.println(estudianteUniversitario1);
        
        Estudiante estudianteUniversitario2 = applicationContext.getBean("estudianteUniversitario", Estudiante.class);
        System.out.println(estudianteUniversitario2);
    }
}
