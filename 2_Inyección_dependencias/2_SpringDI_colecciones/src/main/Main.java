package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.CollectionsManager;

public class Main {

    public static void main(String[] args) {
    	
    	ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        
        CollectionsManager manager = applicationContext.getBean("manager", CollectionsManager.class);

        manager.muestraLista();
        manager.muestraArreglo();
        manager.muestraConjunto();
        manager.muestraMapa();
        manager.muestraPropiedades();
    }
}
