package exceptions;

public class DatabaseException extends Exception {
    public DatabaseException(String string) {
        super(string);
    }
}
