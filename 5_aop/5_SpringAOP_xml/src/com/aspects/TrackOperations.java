package com.aspects;

import org.aspectj.lang.ProceedingJoinPoint;

public class TrackOperations {

	//==================================================
	//ADVICES===========================================
	//==================================================
	
	//Advice se ejecuta antes de la llamada a los m�todos definidos 
	//en el Pointcut allUniversityOperations()
	public void beforeAllOperations(){
		System.out.println("\nAspect @Before message : "
				+ "\n\ttaking actions before University method calling"
				+ "\n\t...");
	}
	
	//Advice se ejecuta despu�s de la llamada a los m�todos definidos 
	//en el Pointcut allUniversityOperations()
	public void afterAllOperations(){
		System.out.println("\nAspect @After message : "
				+ "\n\ttaking actions after University method calling"
				+ "\n\t...\n");
	}
	
	//Advice se ejecuta despu�s de la llamada a los m�todos y antes del return
	//de los m�todos definidos en el Pointcut allStudentGetMethods()
	public void afterAllStudentMethods(Object result){
		System.out.println("\nAspect @AfterReturning message : "
				+ "\n\ttaking actions after Student method calling"
				+ "\n\t...\n");
		
		System.out.println("\nAspect @AfterReturning message : "
				+ "\n\ttaking actions before Student method return"
				+ "\n\t..."
				+ "\n\tmethod returning : " + result.toString() + "\n");
	}
	
	//Advice se ejecuta antes y despu�s de la llamada a los m�todos definidos 
	//en el Pointcut allStudentSetMethods()
	public void aroundAllStudentSetMethods(ProceedingJoinPoint pjp) throws Throwable{
		System.out.println("\nAspect @Around message : "
				+ "\n\tAdditional Concern Before calling Student setXXX() method"
				+ "\n\t...");
		
		Object o = pjp.proceed();
		
		System.out.println("\nAspect @Around message : "
				+ "\n\tAdditional Concern After calling Student setXXX() method"
				+ "\n\t...");
	}
	
	//Advice se ejecuta si alguno de los m�todos definidos en el
	//Pointcut allUniversityTrhowMethods() lanza una excepci�n
	public void allUniversityTrhowMethods(Throwable ex){
		System.err.println("There has been an exception: " + ex.toString());  
	}
	   
}
