package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.Universitario;

public class Main {

    public static void main(String[] args) {
    	
    	ApplicationContext applicationContext = new AnnotationConfigApplicationContext("beans");
        
    	System.out.println("Estudiante 1: " + applicationContext.getBean("estudiante"));
//    	System.out.println("Estudiante 2: " + applicationContext.getBean("estudiante"));
        
    	((AbstractApplicationContext) applicationContext).close();
    }
}
