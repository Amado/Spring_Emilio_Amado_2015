package eventsListeners;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

import customEvents.MyCustomEvent;

/*
 * Creamos este listener para llevar a cabo acciones cuando el evento es escuchado y recibido
 */
public class MyCustomEventListener implements ApplicationListener<MyCustomEvent> {

	@Override
	public void onApplicationEvent(MyCustomEvent event) {
//		event.eventActions();
		System.out.println("MyCustomEventListener says -> MyCustomEvent Received");
	}
}
