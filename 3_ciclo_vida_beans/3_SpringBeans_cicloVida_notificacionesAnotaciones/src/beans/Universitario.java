package beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

@Service(value="estudiante")
public class Universitario implements Estudiante{
	
	public Universitario(){
	    System.out.println("--Construyendo un universitario--");
	}

	@Override
	public int presentaExamen() {
		return (int) (Math.random() * 10.0);
	}
	
	@PostConstruct
	public void inicializa()
	{
	    System.out.println("Realizando inicializacion de rutina en el bean.");
	}
	    
	@PreDestroy
	public void destruye()
	{
	    System.out.println("Realizando destruccion de rutina en el bean.");
	}

}
