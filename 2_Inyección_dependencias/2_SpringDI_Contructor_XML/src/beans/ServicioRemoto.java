package beans;

import org.springframework.stereotype.Service;

public class ServicioRemoto {
	
    private Proceso proceso;
    
    public ServicioRemoto(){
    }

    public ServicioRemoto(Proceso proceso){
        this.proceso = proceso;
    }
    
    public Object consultaDato(){
        return proceso.ejecuta();
    }
    
}
