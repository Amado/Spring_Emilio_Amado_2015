package beans;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Universitario implements Estudiante, InitializingBean, DisposableBean {
	
	public Universitario(){
	    System.out.println("--Construyendo un universitario--");
	}

	@Override //de la interfaz Estudiante
	public int presentaExamen() {
		return (int) (Math.random() * 10.0);
	}

	@Override //de la interfaz DisposableBean
	public void destroy() throws Exception {
		System.out.println("... bean destruido ...");
	}

	@Override //de la interfaz InitializingBean
	public void afterPropertiesSet() throws Exception {
		System.out.println("... bean creado ...");
	}

}
