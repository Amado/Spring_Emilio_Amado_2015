package com.main;

import org.aspectj.lang.annotation.AfterThrowing;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.beans.Student;
import com.beans.University;

public class Main {
	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext(
				"beans.xml");

		University university = (University) context.getBean("university");
//		University university = context.getBean("university", University.class);

		Student student = (Student) context.getBean("student");
		
		//aspect toma acciones en advices:
		//	@Before advice
		//	@After advice
		//	@AfterReturning advice
		university.enrollStudent(student);
		
		//aspect toma acciones en @Around advice 
		student.setName("Mar�a"); 
		
//		System.err.println(student.getName());
		
		//aspect toma acciones en @AfterThrowing advice
		university.throwException();
	}
}
