package beans;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class CollectionsManager {
	
    private List lista;
    private Persona[] arreglo;
    private Set conjunto;
    private Map mapa;
    private Properties propiedades;
    
    //=============================================================
    //METODOS PARA MOSTAR EL CONTENIDO DE LAS DISTINTAS COLECCIONES
    //=============================================================
    
    public void muestraLista(){
        System.out.println("\n---Mostrando lista---");

        for(Object o : lista){
            System.out.println(o.getClass() + ": " + o);
        }
    }
    
    public void muestraArreglo(){
        System.out.println("\n---Mostrando arreglo---");

        for(Object o : arreglo){
            System.out.println(o.getClass() + ": " + o);
        }
    }
    
    public void muestraConjunto(){
        System.out.println("\n---Mostrando conjunto---");

        for (Iterator it = conjunto.iterator(); it.hasNext();){
            Object o = it.next();

            System.out.println(o.getClass() + ": " + o);
        }
    }
    
    public void muestraMapa(){
        System.out.println("\n---Mostrando mapa---");

        for (Iterator it = mapa.keySet().iterator(); it.hasNext();){
            Object o = it.next();

            System.out.println("[llave] " + o.getClass() + ": " + o + ", [valor]" + mapa.get(o).getClass() + ": " + mapa.get(o) );
        }
    }
    
    public void muestraPropiedades(){
        System.out.println("\n---Mostrando propiedades---");

        for (Iterator it = propiedades.keySet().iterator(); it.hasNext();){
            Object o = it.next();

            System.out.println("[llave] " + o.getClass() + ": " + o + ", [valor]" + propiedades.get(o).getClass() + ": " + propiedades.get(o) );
        }
    }
    
    //==============================================================
    //SETTERS DE LAS COLECIONES (vamos a hacer inyecci�n por setter)
    //==============================================================

    public void setArreglo(Persona[] arreglo){
        this.arreglo = arreglo;
    }

    public void setConjunto(Set conjunto){
        this.conjunto = conjunto;
    }

    public void setLista(List lista){
        this.lista = lista;
    }

    public void setMapa(Map mapa){
        this.mapa = mapa;
    }

    public void setPropiedades(Properties propiedades){
        this.propiedades = propiedades;
    }
}
