package beans;

import org.springframework.stereotype.Service;

public class ServicioRemoto {
	
    private Proceso proceso;
    private Integer repeticiones;
    
    public ServicioRemoto(){
    }

    public ServicioRemoto(Proceso proceso){
        this.proceso = proceso;
    }
    
    public Object consultaDato(){
    	String resultado = "";
    	
    	for(int i = 0; i < repeticiones; i++){
    		resultado = resultado + (i+1) + " " + proceso.ejecuta().toString() + "\n";
    	}
    	
        return resultado;
    }
    
    public void setRepeticiones(Integer repeticiones){
     this.repeticiones = repeticiones;
    }
}
