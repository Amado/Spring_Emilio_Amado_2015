package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.Sun;

public class Main {

    public static void main(String[] args) {
    	
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        Sun sun1 = applicationContext.getBean("sun", Sun.class);
        Sun sun2 = applicationContext.getBean("sun", Sun.class);

        System.out.println(sun1.getMensaje());
        System.out.println(sun2.getMensaje());
    }
}
