package eventsListeners;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;


public class ContextCloseEventlistener implements ApplicationListener<ContextClosedEvent> {

	public void onApplicationEvent(ContextClosedEvent event) {
		System.out.println("ContextClosedEvent Received");
	}
}
