package beans;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;

import customEvents.MyCustomEvent;

public class HelloWorld implements ApplicationEventPublisherAware{
	
	   private String message;
	   private ApplicationEventPublisher publisher;

	   public void setMessage(String message){
	      this.message  = message;
	   }
	   
	   public void getMessage(){
	      System.out.println("HelloWorld says ------------> Your Message : " + message);
	      helloWorldPublishEvent();
	   }

	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher publisher) {
		this.publisher = publisher;
	}

	public void helloWorldPublishEvent(){
		System.out.println("HelloWorld says ------------> I will publish a MyCustomEvent if listener bean is created");
		MyCustomEvent mce = new MyCustomEvent(this);
		publisher.publishEvent(mce);
	}
}
