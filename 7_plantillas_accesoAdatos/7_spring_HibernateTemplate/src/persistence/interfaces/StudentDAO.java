package persistence.interfaces;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.orm.hibernate3.HibernateTemplate;

import entities.Student;


public interface StudentDAO {

	public void setHibernateTemplate(HibernateTemplate hibernateTemplate);

	public void insertStudent(Student e);
	
	public void updateStudent(Student e);

	public void deleteStudent(Student e);

	public Student getStudent(int id);

	public List<Student> listStudents();
}
