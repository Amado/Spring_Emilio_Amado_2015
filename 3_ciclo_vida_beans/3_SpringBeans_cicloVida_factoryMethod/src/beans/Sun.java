package beans;

/**
 * Ejemplo de patr�n de dise�o de un objeto singletone :
 * 
 * Como se ve , el constructor es llamado desde un bloque de inicializaci�n est�tico, por lo que
 * queda asegurado que solo podr� haber una instancia de este objeto por muchas veces que se intente 
 * instanciar. Adem�s ya que el constructor es privado, para obtener la instancia creada, se tendr� que 
 * hacer a trav�s del m�todo "getInstancia()" que es "public static".
 * 
 * @author Emilio
 *
 */
public class Sun {
	
    private static Sun instancia;

    static{
        instancia = new Sun();
    }

    private Sun(){  
    	System.out.println("Creando un Sun ...");
    }

    public static Sun getInstancia(){
        return instancia;
    }

    public String getMensaje(){
        return "Hola a todos los desarrolladores Java";
    }
}
