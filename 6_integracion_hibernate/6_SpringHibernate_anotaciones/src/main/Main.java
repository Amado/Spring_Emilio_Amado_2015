package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import entidades.Contacto;
import persistencia.ContactosDAOHibernateImpl;

public class Main {

	public static void main(String[] args) {
		
        ApplicationContext appContext = new ClassPathXmlApplicationContext("applicationContext.xml");
        ContactosDAOHibernateImpl contactosDAO = appContext.getBean("contactoDAO", ContactosDAOHibernateImpl.class);

        long idContactoGuardado = contactosDAO.guardaContacto(new Contacto("Nombre 1", "Email 1", "Telefono 1"));

        Contacto contactoGuardado = contactosDAO.obtenContacto(idContactoGuardado);

        System.out.println("Nombre: " + contactoGuardado.getNombre());
        System.out.println("Email: " + contactoGuardado.getEmail());
	}
}
