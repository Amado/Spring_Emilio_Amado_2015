package beans;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service(value="servicioRemoto")
public class ServicioRemoto {
	
    private Proceso proceso;
    
    public ServicioRemoto(){
    }

    @Autowired
    public ServicioRemoto(@Qualifier("ordenacionEnteros")Proceso proceso){
        this.proceso = proceso;
    }
    
    public Object consultaDato(){
        return proceso.ejecuta();
    }
    
}
