package main;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import entities.Student;
import persistence.StudentHibernateTemplate;

public class Main {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

		StudentHibernateTemplate studentHibernateTemplate = (StudentHibernateTemplate) context
				.getBean("studentHibernateTemplate");
		
		Student zara = new Student();
		zara.setAge(11);
		zara.setName("Zara");
		
		Student nuha = new Student();
		nuha.setAge(2);
		nuha.setName("Nuha");
		
		Student ayan = new Student();
		ayan.setAge(15);
		ayan.setName("Ayan");

		System.out.println("\n------Records Creation--------");
		studentHibernateTemplate.insertStudent(zara);
		studentHibernateTemplate.insertStudent(nuha);
		studentHibernateTemplate.insertStudent(ayan);

		System.out.println("\n------Listing Multiple Records--------");
		List<Student> students = studentHibernateTemplate.listStudents();
		for (Student record : students) {
			System.out.print("ID : " + record.getId());
			System.out.print(", Name : " + record.getName());
			System.out.println(", Age : " + record.getAge());
		}

		System.out.println("\n----Updating Record with ID = 2 -----");
		nuha.setAge(20);
		studentHibernateTemplate.updateStudent(nuha);

		System.out.println("\n----Listing Record with ID = 2 -----");
		Student student = studentHibernateTemplate.getStudent(2);
		System.out.print("ID : " + student.getId());
		System.out.print(", Name : " + student.getName());
		System.out.println(", Age : " + student.getAge());
	}

}
