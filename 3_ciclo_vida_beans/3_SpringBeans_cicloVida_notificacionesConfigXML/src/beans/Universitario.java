package beans;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

public class Universitario implements Estudiante{
	
	public Universitario(){
	    System.out.println("--Construyendo un universitario--");
	}

	@Override
	public int presentaExamen() {
		return (int) (Math.random() * 10.0);
	}
	
	public void preparaEstudiante(){
	    System.out.println("... Estudiante inicia sus estudios ...");
	}
	    
	public void despideEstudiante(){
	    System.out.println("... Estudiante finaliza sus estudios ...");
	}

}
