package customEvents;

import org.springframework.context.ApplicationEvent;

/*
 * Creamos un evento propio, definiendo el conjunto de acciones
 * que deseamos llevar a cabo
 */
public class MyCustomEvent extends ApplicationEvent {

	public MyCustomEvent(Object source) {
		super(source);
		System.out.println("MyCustomEvent says ---------> This event is being called");
	}
	
//	public void eventActions(){
//		System.out.println("MyCustomEvent says ---------> This event is being called");
//	}

}
