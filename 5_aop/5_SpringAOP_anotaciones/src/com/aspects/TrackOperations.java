package com.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;

@Aspect
public class TrackOperations {
	
	//==================================================
	//POINTCUTS=========================================
	//==================================================
	
	//Acciones en respuesta a todos los m�todos del package com.beans. y de la clase University
	@Pointcut("execution(* com.beans.University.*(..))")
	private void allUniversityOperations() {}
	
	//Acciones en respuesta a todos los m�todos de tipo getter en el package
	//com.beans. y de la clase Student
	@Pointcut("execution(* com.beans.Student.get*(..))")
	private void allStudentGetMethods() {}
	
	//Acciones en respuesta a todos los m�todos que lancen excepciones en el
	//package com.beans. y de la clase University
	@Pointcut("execution(* com.beans.University.throw*(..))")
	private void allUniversityTrhowMethods() {}
	
	//Acciones en respuesta a todos los m�todos de tipo setter en el package
	//com.beans. y de la clase Student
	@Pointcut("execution(* com.beans.Student.set*(..))")
	private void allStudentSetMethods() {}
	
	
	//==================================================
	//ADVICES===========================================
	//==================================================
	
	//Advice se ejecuta antes de la llamada a los m�todos definidos 
	//en el Pointcut allUniversityOperations()
	@Before("allUniversityOperations()")
	public void beforeAllOperations(){
		System.out.println("\nAspect @Before message : "
				+ "\n\ttaking actions before University method calling"
				+ "\n\t...");
	}
	
	//Advice se ejecuta despu�s de la llamada a los m�todos definidos 
	//en el Pointcut allUniversityOperations()
	@After("allUniversityOperations()")
	public void afterAllOperations(){
		System.out.println("\nAspect @After message : "
				+ "\n\ttaking actions after University method calling"
				+ "\n\t...\n");
	}
	
	//Advice se ejecuta despu�s de la llamada a los m�todos y antes del return
	//de los m�todos definidos en el Pointcut allStudentGetMethods()
	@AfterReturning(pointcut = "allStudentGetMethods()", returning="result")
	public void afterAllStudentMethods(Object result){
		System.out.println("\nAspect @AfterReturning message : "
				+ "\n\ttaking actions after Student method calling"
				+ "\n\t...\n");
		
		System.out.println("\nAspect @AfterReturning message : "
				+ "\n\ttaking actions before Student method return"
				+ "\n\t..."
				+ "\n\tmethod returning : " + result.toString() + "\n");
	}
	
	//Advice se ejecuta antes y despu�s de la llamada a los m�todos definidos 
	//en el Pointcut allStudentSetMethods()
	@Around("allStudentSetMethods()")
	public void aroundAllStudentSetMethods(ProceedingJoinPoint pjp) throws Throwable{
		System.out.println("\nAspect @Around message : "
				+ "\n\tAdditional Concern Before calling Student setXXX() method"
				+ "\n\t...");
		
		Object o = pjp.proceed();
		
		System.out.println("\nAspect @Around message : "
				+ "\n\tAdditional Concern After calling Student setXXX() method"
				+ "\n\t...");
	}
	
	//Advice se ejecuta si alguno de los m�todos definidos en el
	//Pointcut allUniversityTrhowMethods() lanza una excepci�n
	@AfterThrowing(pointcut = "allUniversityTrhowMethods()", throwing="ex")
	public void allUniversityTrhowMethods(Throwable ex){
		System.err.println("\nAspect @AfterThrowing message : ");
		System.err.println("There has been an exception: " + ex.toString());  
	}
	   
}
