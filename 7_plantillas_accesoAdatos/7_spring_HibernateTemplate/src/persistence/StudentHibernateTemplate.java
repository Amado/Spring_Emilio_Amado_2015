package persistence;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.orm.hibernate3.HibernateTemplate;

import persistence.interfaces.StudentDAO;
import entities.Student;

public class StudentHibernateTemplate implements StudentDAO{

	HibernateTemplate hibernateTemplate;
	
	@Override
	public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
		this.hibernateTemplate = hibernateTemplate;
	}

	// method to save employee
	@Override
	public void insertStudent(Student e) {
		hibernateTemplate.save(e);
		System.out.println("Created Record Name = " + e.getName() + " Age = " + e.getAge());
	}

	// method to update employee
	@Override
	public void updateStudent(Student e) {
		hibernateTemplate.update(e);
		System.out.println("Updated Record with ID = " + e.getId());
	}

	// method to delete employee
	@Override
	public void deleteStudent(Student e) {
		hibernateTemplate.delete(e);
		System.out.println("Deleted Record with ID = " + e.getId());
	}

	// method to return one employee of given id
	@Override
	public Student getStudent(int id) {
		Student e = (Student) hibernateTemplate.get(Student.class, id);
		System.out.println("Updated Record with ID = " + id);
		return e;
	}

	// method to return all employees
	@Override
	public List<Student> listStudents() {
		List<Student> list = new ArrayList<Student>();
		list = hibernateTemplate.loadAll(Student.class);
		return list;
	}

}
