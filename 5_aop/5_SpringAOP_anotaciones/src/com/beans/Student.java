package com.beans;

public class Student {

	private Integer age;
	private String name;
	private String ssNumber;
	private boolean isEnrolled;
	private boolean isLicensed;

	public Student(Integer age, String name, String ssNumber) {
		super();
		this.age = age;
		this.name = name;
		this.ssNumber = ssNumber;
	}

	public void setAge(Integer age) {
		System.err.println("\n\tStudent message : setter method called");
		this.age = age;
	}

	public Integer getAge() {
		return age;
	}

	public void setName(String name) {
		System.err.println("\n\tStudent message : setter method called");
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setSsNumber(String ssNumber) {
		System.err.println("\n\tStudent message : setter method called");
		this.ssNumber = ssNumber;
	}

	public String getSsNumber() {
		return ssNumber;
	}

	public boolean isEnrolled() {
		return isEnrolled;
	}

	public void setEnrolled(boolean isEnrolled) {
		this.isEnrolled = isEnrolled;
	}

	public boolean isLicensed() {
		return isLicensed;
	}

	public void setLicensed(boolean isLicensed) {
		this.isLicensed = isLicensed;
	}


}
