package persistencia;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import entidades.Contacto;
import persistencia.interfaces.ContactosDAO;

public class ContactosDAOHibernateImpl implements ContactosDAO {

	private Session sesion;

	private SessionFactory sessionFactory;

	// Spring se encargar� de crear el SessionFactory, por lo que necesita de un setter
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void actualizaContacto(Contacto contacto) throws HibernateException {
		try {
			iniciaOperacion();
			sesion.update(contacto);
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
//			sesion.close();
		}
	}

	@Override
	public void eliminaContacto(Contacto contacto) throws HibernateException {
		try {
			iniciaOperacion();
			sesion.delete(contacto);
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
//			sesion.close();
		}
	}

	@Override
	public long guardaContacto(Contacto contacto) throws HibernateException {
		long id = 0;
		try {
			iniciaOperacion();
			id = (Long) sesion.save(contacto);
		} catch (HibernateException he) {
			manejaExcepcion(he);
			throw he;
		} finally {
//			sesion.close();
		}

		return id;
	}

	@Override
	public Contacto obtenContacto(long idContacto) throws HibernateException {
		Contacto contacto = null;
		try {
			iniciaOperacion();
			contacto = (Contacto) sesion.get(Contacto.class, idContacto);
		} finally {
//			sesion.close();
		}

		return contacto;
	}

	@Override
	public List<Contacto> obtenListaContactos() throws HibernateException {
		List<Contacto> listaContactos = null;

		try {
			iniciaOperacion();
			listaContactos = sesion.createQuery("from Contacto").list();
		} finally {
//			sesion.close();
		}

		return listaContactos;
	}

	// Inicia la session de Hibernate y una transacci�n sobre la DB
	private void iniciaOperacion() throws HibernateException {
		sesion = sessionFactory.getCurrentSession();
	}


	private void manejaExcepcion(HibernateException he)
			throws HibernateException {
		throw new HibernateException(
				"Ocurrió un error en la capa de acceso a datos", he);
	}

}
