package main;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

import beans.HelloWorld;

public class Main {

	public static void main(String[] args) {
		
		ConfigurableApplicationContext  context = new AnnotationConfigApplicationContext
				("beans", "eventsListeners");

		HelloWorld helloWorld =   context.getBean("helloWorld", HelloWorld.class);
		helloWorld.getMessage();
	}
}
