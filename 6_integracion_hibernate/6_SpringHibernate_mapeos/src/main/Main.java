package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import entidades.Contacto;
import persistencia.ContactosDAOHibernateImpl;

public class Main {

	public static void main(String[] args) {
		
		//cargamos la config de "applicationCOntext.xml"
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext(
				"applicationContext.xml");

		//obtnemos el bean que gestiona los contactos
		ContactosDAOHibernateImpl contactosDAOHibernateImpl = applicationContext.getBean("contactoDAO",
				ContactosDAOHibernateImpl.class);
		
		//guardamos un contacto en la db de MySQL
		Contacto contacto1 = new Contacto("emilio", "xxx@gmail.com", "123456789");
		long idContacto1 = contactosDAOHibernateImpl.guardaContacto(contacto1);
		
		//recuperamos el contacto y lo imprimimos
		Contacto contactoRecuperado = contactosDAOHibernateImpl.obtenContacto(idContacto1);
		System.out.println("Nombre: " + contactoRecuperado.getNombre());
	    System.out.println("Email: " + contactoRecuperado.getEmail());
	}
}
