package main;

import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.ConfigurableApplicationContext;

import beans.HelloWorld;

public class Main {

	public static void main(String[] args) {
		
		ConfigurableApplicationContext  context = new ClassPathXmlApplicationContext
				("applicationContext.xml");
		
		
		context.start();

		HelloWorld obj = context.getBean("helloWorld", HelloWorld.class);
		obj.getMessage();
		
		//Esto no funcionar� en ning�n caso ya que es imposible para Spring buscar la dependencia 
		//de una clase an�nima
//		context.addApplicationListener(new ApplicationListener<ContextClosedEvent>() {
//			
//			public void onApplicationEvent(ContextClosedEvent event) {
//				System.out.println("ContextClosedEvent Received");
//			}
//		});
		
		context.refresh();
		context.stop();
		context.close();
	
	}
}
