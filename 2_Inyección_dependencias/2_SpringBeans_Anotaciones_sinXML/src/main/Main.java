package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import beans.ServicioRemoto;

public class Main {

    public static void main(String[] args) {
    	
    	ApplicationContext applicationContext = new AnnotationConfigApplicationContext(
    			"beans" //ruta del package donde se encuentran los beans
    			); 
        
        //con nuestro "ApplicationContext" podemos obtener un bean haciendo uso del identificador que 
        //le dimos en el archivo XML, en este caso "servicioRemoto"
        ServicioRemoto servicio = applicationContext.getBean("servicioRemoto", ServicioRemoto.class);
        
        //Ahora que ya tenemos una instancia de "ServicioRemoto" podemos invocar sus m�todos como lo hacemos normalmente
        System.out.println("El valor es " + servicio.consultaDato());
    }
}
