package main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import beans.HelloWorld;

public class Main {

	public static void main(String[] args) {
		
//		HelloWorld helloWorld = new HelloWorld();
//		helloWorld.setMessage("Hello World! from self instance");
//		helloWorld.getMessage();
		
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"applicationContext.xml");

		HelloWorld obj = (HelloWorld) context.getBean("helloWorld");

		obj.getMessage();
		
	}

}
