package beans;

public class Universitario implements Estudiante {
	
	public Universitario(){
	    System.out.println("--Construyendo un universitario--");
	}

	@Override
	public int presentaExamen() {
		return (int) (Math.random() * 10.0);
	}

}
